from machine import Pin, I2C, SPI, ADC
import machine
import time
#import upip # and download uqtt driver
#upip.install('micropython-umqtt.robust')
from umqtt.robust import MQTTClient
import ssd1306
import BME280
import network
import opcr1
import opcn2
#import wifi_manager
import wifimgr
try:
  import usocket as socket
except:
  import socket

### BOARD SETUP 

# External button (future use?)
# BUTTON = machine.Pin(4, machine.Pin.IN, machine.Pin.PULL_UP)

# SPI2 for display and oled instance
spi2 = SPI(-1, baudrate=500000, polarity=0, phase=0, sck=Pin(17), mosi=Pin(5), miso=Pin(4))
oled = ssd1306.SSD1306_SPI(128, 64, spi2, machine.Pin(15), machine.Pin(0), machine.Pin(16)) #data res cs

# SPI1 for particle meter and particle meter settings
spi1 = machine.SPI(2, baudrate=500000, polarity=0, phase=1, bits=8, firstbit=0, sck=machine.Pin(18), mosi=machine.Pin(23), miso=machine.Pin(19))
CS = machine.Pin(21, machine.Pin.OUT, machine.Pin.PULL_UP)
#particles = opcr1.OPCR1(spi1, CS,oled)
particles = opcn2.OPCN2(spi1, CS,oled)
# Sampling parameters
DELAY_BETWEEN_SAMPLING_MS = 1000  # miliseconds
NUMBER_OF_SAMPLES = 7             # number of samplings
NUMBER_OF_OUTLIERS = 1            # after sampling, those samples will be cut off from both sides (filter)
LOOP_DELAY_S =  300               # seconds

# ADC for soil humidity meter
adc = ADC(Pin(32))          # create ADC object on ADC pin
adc.atten(ADC.ATTN_11DB)

# i2c bme280 temp and pressure sensor
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=200000)
#print(i2c.scan())
bme = BME280.BME280(i2c=i2c,address = 0x76)

print("Board setup done")

### NETWORK SETUP

wlan = wifimgr.get_connection()
if wlan is None:
    print("Could not initialize the network connection.")
    while True:
        pass  # you shall not pass :D

print("Network setup done")

### MQTT CREDENTIALS ETC
#ADAFRUIT_IO_URL = b'io.adafruit.com' 
#ADAFRUIT_USERNAME = b'i_a'
#ADAFRUIT_IO_KEY = b'aio_Furn85vJN8w8jOq1WN8CLfeAOvZr'
# feeds (or topics?):
#if(N2):
#  ADAFRUIT_IO_FEEDNAME_PM1 = b'pm1_N2'
#  ADAFRUIT_IO_FEEDNAME_PM25 = b'pm25_N2'
#  ADAFRUIT_IO_FEEDNAME_PM10 = b'pm10_N2'
#if (R2):
#  ADAFRUIT_IO_FEEDNAME_PM1 = b'pm1_R1'
#  ADAFRUIT_IO_FEEDNAME_PM25 = b'pm25_R1'
#  ADAFRUIT_IO_FEEDNAME_PM10 = b'pm10_R1'
# create a random MQTT clientID (urandom import needed)
#random_num = int.from_bytes(os.urandom(3), 'little')
#bytes('client_'+str(random_num), 'utf-8')
# or in python3 -c 'from uuid import uuid4; print(uuid4())'
#mqtt_client_id = '2faaca32-c358-406b-ae99-8aae3dd7eed5'
#client = MQTTClient(client_id=mqtt_client_id, server=ADAFRUIT_IO_URL, user=ADAFRUIT_USERNAME, password=ADAFRUIT_IO_KEY, ssl=False)

client = MQTTClient('2faaca32-c358-406b-ae99-8aae3dd7eed5', '192.168.50.125', user="klient", password="DVANACTstrom")  # Server with MQTT broker is running here! #158.196.200.247
#----client.connect()

try:            
    client.connect()
except Exception as e:
    print('could not connect to MQTT server {}{}'.format(type(e).__name__, e))
    sys.exit()
print("MQTT client setup done")


def display_out(pm1,pm25,pm10):
  oled.fill(0)
  oled.text('Published values:'  , 0, 0)
  oled.text('pm1: '  , 0, 15)
  oled.text(str(pm1), 60, 15)
  oled.text('pm2.5:', 0, 25)
  oled.text(str(pm25), 60, 25)
  oled.text('pm10:' , 0, 35)
  oled.text(str(pm10) , 60, 35)
  oled.show()


def print_out(value1, value2, value3, value4):
  print("pm1: " + str(value1) + "\tpm2.5: " + str(value2) + "\tpm10: " + str(value3) + "\tsoil_hum: " + str(value4))


oled.fill(0)
oled.text('ON AIR'  , 0, 0)
oled.show()

while True:
  particles.switch_on()
  time.sleep_ms(500)
  particles.read_values()
  print('\r\n\r\nFlushed.')

  pm1  =  [None] * NUMBER_OF_SAMPLES
  pm25 =  [None] * NUMBER_OF_SAMPLES
  pm10 =  [None] * NUMBER_OF_SAMPLES

  print('->Sampling...')

  for i in range (0,NUMBER_OF_SAMPLES):
    #wait while sampling
    time.sleep_ms(DELAY_BETWEEN_SAMPLING_MS)
    #read_histogram()
    pm1[i], pm25[i], pm10[i] = particles.read_values()
    #print_out(pm1[i], pm25[i],pm10[i])

  pm1.sort()
  pm25.sort()
  pm10.sort()

  print('-->Cutting off outliers, calculating around center:')

  pm1_med  = 0
  pm25_med = 0
  pm10_med = 0

  for i in range(NUMBER_OF_OUTLIERS, (1+NUMBER_OF_SAMPLES-2*NUMBER_OF_OUTLIERS)):
    #print_out(pm1[i], pm25[i], pm10[i])
    pm1_med  += pm1 [i]
    pm25_med += pm25[i]
    pm10_med += pm10[i]

  pm1_med  = pm1_med  / (NUMBER_OF_SAMPLES-2*NUMBER_OF_OUTLIERS)
  pm25_med = pm25_med / (NUMBER_OF_SAMPLES-2*NUMBER_OF_OUTLIERS)
  pm10_med = pm10_med / (NUMBER_OF_SAMPLES-2*NUMBER_OF_OUTLIERS)

  print("--->Reading soil humidity ")
  soil_hum = adc.read()                  # read value, 0-4095 across voltage range 0.0v - 1.0v

  temp = bme.temperature
  time.sleep_ms(300)
  hum = bme.humidity
  time.sleep_ms(300)
  pres = bme.pressure
  print('\r\nTemperature: ', temp)
  print('Humidity: ', hum)
  print('Pressure: ', pres)


  print("--->Publishing ")
  print_out(pm1_med, pm25_med, pm10_med, soil_hum)
  client.publish('panel/balkon/pm1', str(pm1_med))
  client.publish('panel/balkon/pm25', str(pm25_med))
  client.publish('panel/balkon/pm10', str(pm10_med))
  client.publish('panel/balkon/soil', str(soil_hum))
  client.publish('panel/balkon/temp', str(temp))
  client.publish('panel/balkon/hum', str(hum))
  client.publish('panel/balkon/pres', str(pres))
  '''
  display_out(pm1_med, pm25_med, pm10_med)
  mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM1), 'utf-8')
  client.publish(mqtt_feedname, bytes(str(pm1_med), 'utf-8'), qos=0) 
  mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM25), 'utf-8')
  client.publish(mqtt_feedname, bytes(str(pm25_med), 'utf-8'), qos=0) 
  mqtt_feedname = bytes('{:s}/feeds/{:s}'.format(ADAFRUIT_USERNAME, ADAFRUIT_IO_FEEDNAME_PM10), 'utf-8')
  client.publish(mqtt_feedname, bytes(str(pm10_med), 'utf-8'), qos=0) 
  '''

  particles.switch_off()
  time.sleep(LOOP_DELAY_S)

