#  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #  # 
#  Modify where you have your device, my is accessible through the port /dev/ttyUSB0  #
#  (Also don't forget to update rules in your etc/udev/rules.d/ folder                #
#  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #  #

PORT = /dev/ttyUSB0


go: 
	ampy -p $(PORT) -d 1 put sources/boot.py
	ampy -p $(PORT) -d 1 put sources/main.py
	ampy -p $(PORT) -d 1 put sources/ssd1306.py
	ampy -p $(PORT) -d 1 put sources/robust.py
	ampy -p $(PORT) -d 1 put sources/BME280.py
	ampy -p $(PORT) -d 1 put sources/opcn2.py
	ampy -p $(PORT) -d 1 put sources/opcr1.py
	ampy -p $(PORT) -d 1 put sources/wifimgr.py
#	ampy -p $(PORT) -d 1 mkdir /lib
#	ampy -p $(PORT) -d 1 mkdir /lib/umqtt
#	ampy -p $(PORT) -d 1 put sources/robust.py /lib/umqtt/robust.py

reflash: 
	esptool.py -p $(PORT) erase_flash
	esptool.py --chip esp32 --port $(PORT) --baud 921600 \
	write_flash -z 0x1000 esp32-idf3-20200114-v1.12-63-g1c849d63a.bin

reflash8266: 
	esptool.py -p /dev/ttyUSB0 erase_flash
	esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20191220-v1.12.bin

<<<<<<< HEAD
reflash8266_v2:
	esptool.py -p /dev/ttyUSB0 --baud 115200 write_flash --flash_mode=dio --flash_size=4MB 0 esp8266-20191220-v1.12.bin

=======
>>>>>>> opc
erase: 
	ampy -p $(PORT) rm main.py
	ampy -p $(PORT) rm boot.py

ls: 
	ampy -p /dev/ttyUSB0 ls



#esptool.py --chip esp32 --port /dev/ttyUSB1 --baud 921600 write_flash -z 0x1000 esp32-idf3-20200114-v1.12-63-g1c849d63a.bin 
#esptool.py -p /dev/ttyUSB1 --baud 921600 erase_flash

